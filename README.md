
Provided files:
* run11.Rout: report of the code run to produce the data on which Figure 3 is based
* TV2D-data.R: cleaned-up and commented version of the code run  to produce the data
* resultTheor.RData: the data on which Figure 3 is based
* TV2D-fig.R: the code used to produce Figure 3 from resultTheor.RData

THIS SOURCE CODE IS SUPPLIED “AS IS”  WITHOUT WARRANTY OF ANY KIND, AND ITS AUTHOR AND THE JOURNAL OF MACHINE LEARNING RESEARCH (JMLR)  AND JMLR’S PUBLISHERS AND DISTRIBUTORS, DISCLAIM ANY AND ALL WARRANTIES, INCLUDING BUT NOT LIMITED TO ANY IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE, AND ANY WARRANTIES OR NON INFRINGEMENT. THE USER ASSUMES ALL LIABILITY AND RESPONSIBILITY FOR USE OF THIS SOURCE CODE, AND NEITHER THE AUTHOR NOR JMLR, NOR JMLR’S PUBLISHERS AND DISTRIBUTORS, WILL BE LIABLE FOR DAMAGES OF ANY KIND RESULTING FROM ITS USE.
Without limiting the generality of the foregoing, neither the author, nor JMLR, nor JMLR’s publishers and distributors,  warrant that the Source Code will be error-free, will operate without interruption, or will meet the needs of the user.
